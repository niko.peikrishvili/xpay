<?php

namespace xpay\gateways;

use \xpay\common\Requests\Refund;
use \xpay\common\Responses\RefundResponse;

/**
 * Description of TwoCheckoutGateway
 *
 * @author niko.peikrishvili
 */
class TwoCheckoutGateway implements \xpay\common\GatewayInterface {

    protected $resons = [
        '1' => 'Did not receive order',
        '2' => 'Did not like item',
        '3' => 'Item(s) not as described',
        '4' => 'Fraud',
        '5' => 'Other',
        '6' => 'Item not available',
        '7' => 'Do Not Use (Internal use only)',
        '8' => 'No response from merchant',
        '9' => 'Recurring last installment',
        '10' => 'Cancellation',
        '11' => 'Billed in error',
        '12' => 'Prohibited product',
        '13' => 'Service refunded at merchants request',
        '14' => 'Non delivery',
        '15' => 'Not as described',
        '16' => 'Out of stock',
        '17' => 'Duplicate',
    ];

    
    /**
     * 
     * @param array $config
     * 
     * $param = [
     *      @param private_key   - privete key from 2Checkout
     *      @param seller_id     - Seller Id provided by 2Checkout
     *      @param username      -   Username
     *      @param password      -   Password
     * ]
     */
    public function __construct(array $config) {
        \Twocheckout::privateKey($config["private_key"]);
        \Twocheckout::sellerId($config['seller_id']);

        \Twocheckout::username($config['username']);
        \Twocheckout::password($config['password']);
        \Twocheckout::verifySSL(false);  // this is set to true by default
        // To use your sandbox account set sandbox to true
        \Twocheckout::sandbox(true);

        // All methods return an Array by default or you can set the format to 'json' to get a JSON response.
        \Twocheckout::format('json');
    }

    public function balance() {
        
    }

    public function cancelSusbscription(array $params) {
        
    }

    public function pay(array $params) {
        
    }

    public function refund(Refund $refund): RefundResponse {
        if(!key_exists($refund->comment, $this->resons))
        {
            throw new \InvalidArgumentException("comment is invalid, it whould be category ID");
        }
        $params = array(
            'sale_id' => $refund->referenceId,
            'category' => $refund->comment,
            'comment' => $this->resons[$refund->comment]
        );
        try {
            $refundResult = \Twocheckout_Sale::refund($params);
            
            $refundResponse = new RefundResponse();
            if($refundResult[''])
            return $refundResponse;
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function startSusbscription(array $params) {
        
    }

}
