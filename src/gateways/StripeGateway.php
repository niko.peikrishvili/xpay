<?php

namespace xpay\gateways;

use \xpay\common\GatewayInterface;
use \xpay\common\Requests\Refund;
use \xpay\common\Responses\RefundResponse;
use \Exception;

/**
 * Description of StripeGateway
 *
 * @author niko.peikrishvili
 */
class StripeGateway implements GatewayInterface {

    /**
     * 
     * @param type $config
     * $param = [
     *  @property apiKey API Key from Stripe
     * 
     * ]
     */
    public function __construct($config) {
        \Stripe\Stripe::setApiKey($config['apikey']);
    }

    /**
     * Charge client Credit Card 
     * @link https://stripe.com/docs/api/php#create_charge
     * @param array $params = [
     *      'amount'        => (integer) Amount for charge. Required.
     *      'currency'      => (string) 3-letter ISO code for currency. Required.
     *      'card'          => (string) Token of user Credit Card returned by Stripe.js . Required
     *      'description'   => (string) Description of charge. Required.
     * 
     * ]
     * @return Stripe\Charge
     */
    public function pay(array $params) {
        $pay = \Stripe\Charge::create($params);
        return $pay;
    }

    /**
     * Partial or full refund of previous charge
     * @link https://stripe.com/docs/api/php#create_refund
     * 
     * @param Refund $refund
     * @return RefundResponse
     * @throws Exception
     */
    public function refund(Refund $refund): RefundResponse {
        try {
            // check if comment exists, and if exists it must be value from
            // predefined list
            if (!empty($refund->comment) && !in_array($refund->comment, ['dublicate', 'fraudelent', 'requested_by_customer'])) {
                throw new Exception("invalid value in comment must be one of 'duplicate', 'fraudulent' or 'requested_by_customer'");
            }
            // normalize amount for Stripe
            if (!empty($refund->amount)) {
                $refund->amount = $refund->amount / 100;
            }
            // send charge request
            $params = ['charge' => $refund->referenceId, 'amount' => $refund->amount, 'reason' => $refund->comment];
            $result = \Stripe\Refund::create($params);
            
            // construct RefundResponse
            $refundResponse = new RefundResponse();
            $refundResponse->referenceId = $result->id;
            $refundResponse->amount = $refund->amount;
            $refundResponse->status = $result->status;
            return $refundResponse;
        } catch (Exception $ex) {
            throw new Exception($ex->getMessage());
        }
    }

    public function cancelSusbscription(array $params) {
        
    }

    public function startSusbscription(array $params) {
        
    }

    /**
     * Retrieve balance information from Stripe
     * @link https://stripe.com/docs/api/php#retrieve_balance
     * @return Stripe\Balance
     */
    public function balance() {
        return \Stripe\Balance::retrieve();
    }

}
