<?php
namespace xpay\common\Requests;
/**
 * Description of Refund
 *
 * @author niko.peikrishvili
 */
class Refund {
    /**
     * Reference ID of transaction
     * @var type 
     */
    public $referenceId;
    /**
     * Amount to Refund
     * @var type 
     */
    public $amount;
    /**
     * Refund Comment
     * @var type 
     */
    public $comment;
    /**
     * Currency ID
     * @var type 
     */
    public $currency;
}
