<?php
namespace xpay\common\Responses;
/**
 * Description of RefundResponse
 *
 * @author niko.peikrishvili
 */
class RefundResponse {
    /**
     * Reference ID of transaction
     * @var type 
     */
    public $referenceId;
    /**
     * Amount to Refund
     * @var type 
     */
    public $amount;
    /**
     * Refund Comment
     * @var type 
     */
    public $comment;
    /**
     * Refund status
     * @var type 
     */
    public $status;
}
