<?php
namespace xpay\common;
/**
 *
 * @author niko.peikrishvili
 */
interface GatewayInterface {
    
    public function __construct($config);
    public function pay(array $params);
    public function refund(Requests\Refund $refund) : Responses\RefundResponse;
    public function startSusbscription(array $params);
    public function cancelSusbscription(array $params);
    public function balance();
}
