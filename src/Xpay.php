<?php

namespace xpay;

/**
 * Description of Xpay
 *
 * @author niko.peikrishvili
 */
final class Xpay {
    private $methods = [];
    
    public function __construct(array $methods) {
        $this->methods = $methods;
    }
    
    public function __get($method)
    {
        if (!key_exists($method, $this->methods) || !$this->methods[$method] instanceof common\GatewayInterface) {
            throw new Exception("Invalid method name");
        }
        return $this->methods[$method];
    }
}
