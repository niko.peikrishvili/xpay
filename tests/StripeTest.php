<?php

use PHPUnit\Framework\TestCase;

class StripeTest extends TestCase {

    const apikey = 'sk_test_auejZzr6Di72AWauDYqePYoW';
    protected $xpay;

    public function setUp()
    {
        $this->xpay = new xpay\Xpay(['stripe'=>new xpay\gateways\StripeGateway(['apikey'=>self::apikey])]);
    }

    
//    public function testPayRequest() {
//        $token = \Stripe\Token::create(
//                ['card' => [
//                    'exp_month' => 10,
//                    'cvc' => 314,
//                    'exp_year' => 2020,
//                    'number' => '4242424242424242',
//                ]
//            ]);
//        $pay = $this->xpay->stripe->pay(['amount' => 1015, 'currency' => 'USD','card'=>$token->id]);
//        $this->assertInstanceOf('Stripe\Charge',$pay);
//        $this->assertEquals('succeeded',$pay->status);
//    }
    
    /**
     * @test
     */
    public function check_refund_only_with_refence_id()
    {
        $token = \Stripe\Token::create(
                ['card' => [
                    'exp_month' => 10,
                    'cvc' => 314,
                    'exp_year' => 2020,
                    'number' => '4242424242424242',
                ]
            ]);        
        $pay = $this->xpay->stripe->pay(['amount' => 1015, 'currency' => 'USD','card'=>$token->id]);
        $refund = new xpay\common\Requests\Refund();
        $refund->referenceId = $pay->id;

        $refundRequest = $this->xpay->stripe->refund($refund);
        $this->assertInstanceOf(xpay\common\Responses\RefundResponse::class,$refundRequest);
        $this->assertEquals('succeeded',$refundRequest->status);
    }

}
