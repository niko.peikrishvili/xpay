<?php
use PHPUnit\Framework\TestCase;

/**
 * Description of TwoCheckoutTest
 *
 * @author niko.peikrishvili
 */
class TwoCheckoutTest extends TestCase{
   
    public function setUp()
    {
        $this->xpay = new xpay\Xpay(['twocheckout'=>new \xpay\gateways\TwoCheckoutGateway([
            'private_key'=>'7AB926D469648F3305AE361D5BD2C3CB',
            'seller_id'=>'1817037',
            'username'=>'testlibraryapi901248204',
            'password'=>'testlibraryapi901248204PASS',
            ])]);
    }
    /**
     * @test
     */
    public function check_if_refunc_success()
    {
        $refund = new xpay\common\Requests\Refund();
        $refund->referenceId = '4774380224';
        $refund->comment = 1;
        $this->xpay->twocheckout->refund($refund);
    }
}
